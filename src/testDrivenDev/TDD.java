package testDrivenDev;

public class TDD {
	
	public static String FizzBuzz(int nFizz,int nBuzz,int test){
		String res = new String();
		if (isDivisible(test,nFizz)){
			res = "Fizz";
		}
		else if (isDivisible(test,nBuzz)){
			res = "Buzz";
		}
		else if(isDivisible(test,nFizz) && isDivisible(test,nBuzz)){
			res = "FizzBuzz";
		}
		else{
			res = "test";
		}
		return res;
	}
	public static boolean isDivisible(int n,int m){
		return n % m == 0;
	}

}
